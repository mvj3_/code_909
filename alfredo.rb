require "rubygems"
require 'alfredo'
#set up the workflow
workflow = Alfredo::Workflow.new

query_str = "#{query}"
query_str = query_str.gsub(/\s+/,"")

case query_str
when /new/
  workflow << Alfredo::Item.new(:title => "new",
                                  :subtitle => 'bar',
                                  :arg => 'baz',
                                  :uid => 123,
                                  :icon_path => 'icon.png',
                                  :icon_type => 'fileicon',
                                  :type => 'file',
                                  :autocomplete => 'moo')
when /hot/
  workflow << Alfredo::Item.new(:title => 'hot',
                                  :subtitle => 'bar',
                                  :arg => 'baz',
                                  :uid => 123,
                                  :icon_path => 'icon.png',
                                  :icon_type => 'fileicon',
                                  :type => 'file',
                                  :autocomplete => 'moo')
when "pop"
  
when /add\s(.+)/
  
else
  workflow << Alfredo::Item.new(:title => "#{query_str.class} - #{query_str}",
                                  :subtitle => 'bar',
                                  :arg => 'baz',
                                  :uid => 123,
                                  :icon_path => 'icon.png',
                                  :icon_type => 'fileicon',
                                  :type => 'file',
                                  :autocomplete => 'moo')
end


# you can add as many items as you want like this:


workflow << Alfredo::Item.new(:title => "Tagskill指令说明",
                                :subtitle => 'new-最新文章; pop-最近热门; hot-热门文章; add URL-添加文章',
                                :arg => 'baz',
                                :uid => 123,
                                :icon_path => 'icon.png',
                                :autocomplete => 'moo')                

# At the end of your script call:
workflow.output!